## INTRODUCTION ##

Author: **Nikita Sineok**
 * Drupal: https://www.drupal.org/u/nikita_tt

Maintainer: **Andrii Sakhaniuk**
 * Drupal: https://www.drupal.org/u/nnevill

Sponsored by: **DevBranch**
 * Drupal : https://www.drupal.org/devbranch
 * Website : https://dev-branch.com/

This module integrates the Viral Loops e-Commerce Referral template with Drupal.

## INSTALLATION ##

See https://www.drupal.org/documentation/install/modules-themes/modules-8
for instructions on how to install or update Drupal modules.

## CONFIGURATION ##

1. Go to Administration  → Commerce → Configuration → Viral Loops (/admin/commerce/config/viral-loops).
2. Fill-in your campaign ID and campaign API token.
3. Set module permissions.
